#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <omp.h>

const double epsilon = 0.00001;

void printVect(double* x, int size) {
    std::cout << '\n';
    for(int i = 0; i < size ; ++ i)
        std::cout << x[i] << '\n';
}

void init(double* &x, double* &A, double* &b, double* &Ax, double* &Ay, int size) {
    ///////////////////////////////////////////////////////////////////////////////////
    srand(13243);
    double max = 2.1, min = -2.8;


    A = new double[size * size];
    for (int i = 0; i < size; ++i) {
        for (int j = i; j < size; ++j) {
            if (j == i) {
                A[i * size + j] = (double)rand()/(double) RAND_MAX*(max-min)+min;
                A[i * size + j] = A[i * size + j]*A[i * size + j]+100.0;
            } else
                A[i * size + j] = (double)rand()/(double) RAND_MAX*(max-min)+min;
            A[j * size + i] = A[i * size + j];
        }
    }

    double *u = new double[size];
    b = new double[size];
    for (int i = 0; i < size; ++i)
        u[i] = sin((2.0*3.1415926535*i)/(double)size);

    std::cout << "\nVector X should look like this:\n";
    printVect(u, size);
    std::cout << "End of vector.\n";
    /**
     *  mul(A, u, b, size);
     */
    //////////////////////////////////////
    for (int i = 0; i < size; ++i) {
        b[i] = 0.0f;
        for (int j = 0; j < size; ++j) {
            b[i] += A[i * size + j] * u[j];
        }
    }
    //////////////////////////////////////

    x = new double [size];
    for(int i = 0; i < size; ++i) {
        x[i] = 0.0;
    }
    ///////////////////////////////////////////////////////////////////////////////////
    Ax = new double [size];
    Ay = new double [size];
    ///////////////////////////////////////////////////////////////////////////////////
    delete[] u;
}

int main(int argc, char *argv[]) {
    int size = 1024; //Размер матрицы и вектора
    double* A; //часть матрицы коэффицентов для каждого процесса она своя
    double* b; //вектор правых частей
    double* x; //вектор значений

    double* Ax; //вспомогательный вектор, хранит в себе результат умножения матрицы A на вектор x
    double* Ay; //вспомогательный вектор, хранит в себе результат умножения матрицы A на вектор y
    ///////////////////////////////////////////////////////////////////////////////////
    init(x, A, b, Ax, Ay, size);
    ///////////////////////////////////////////////////////////////////////////////////

    int iter = 0;
    bool ending;

    double* tmpVector = new double [size];
    double *y = new double [size];
    double div;

    struct timespec start, finish;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    #pragma omp parallel
    {
        while (!ending && iter < 6000) {
            //check end
            //////////////////////////////////////////////////////////////////////
            //mul(A, x, Ax, size);
            #pragma omp for
            for (int i = 0; i < size; ++i) {
                Ax[i] = 0.0;
                for (int j = 0; j < size; ++j) {
                    Ax[i] += A[i * size + j] * x[j];
                }
            }
            /////////////////////////////////////////
            //vectorSub(Ax, b, tmpVector, 1, size);
            #pragma omp for
            for (int i = 0; i < size; ++i)
                tmpVector[i] = Ax[i] - 1 * b[i];
            /////////////////////////////////////////
            //div = (vectorAbs(tmpVector, size) / vectorAbs(b, size));

            double firstABS = 0.0;
            #pragma for shared(firstABS) reduction(+:firstABS)
            for (int i = 0; i < size; ++i)
                firstABS += tmpVector[i] * tmpVector[i];

            double secondABS = 0.0;
            #pragma  for shared(secondABS) reduction(+:secondABS)
            for (int i = 0; i < size; ++i)
                secondABS += b[i] * b[i];
            /////////////////////////////////////////
            #pragma master
            {
                div = firstABS/secondABS;
                ending = (div < epsilon * epsilon);
            }
            //////////////////////////////////////////////////////////////////////
            //vectorSub(Ax, b, y, 1, size);
            #pragma omp for
            for (int i = 0; i < size; ++i)
                y[i] = Ax[i] - 1 * b[i];
            /////////////////////////////////////////
            //mul(A, y, Ay, size);
            #pragma omp for
            for (int i = 0; i < size; ++i) {
                Ay[i] = 0.0;
                for (int j = 0; j < size; ++j) {
                    Ay[i] += A[i * size + j] * y[j];
                }
            }
            /////////////////////////////////////////
            //t = scalarMul(y, Ay, size) / scalarMul(Ay, Ay, size);
            double firstScalarMul = 0.0;
            #pragma  for shared(firstScalarMul) reduction(+:firstScalarMul)
            for (int i = 0; i < size; ++i)
                firstScalarMul += Ay[i] * y[i];

            double secondScalarMul = 0.0;
            #pragma  for shared(secondScalarMul) reduction(+:secondScalarMul)
            for (int i = 0; i < size; ++i)
                secondScalarMul += Ay[i] * Ay[i];
            /////////////////////////////////////////
            double t = firstScalarMul/secondScalarMul;
            /////////////////////////////////////////

            //vectorSub(x, y, tmpVector, t, size);
            #pragma omp for
            for (int i = 0; i < size; ++i)
                tmpVector[i] = x[i] - t * y[i];
            /////////////////////////////////////////
            //copyFirstVectorToSecond(tmpVector, x, size);
            #pragma omp for
            for (int i = 0; i < size; i++)
                x[i] = tmpVector[i];
            //////////////////////////////////////////////////////////////////////
            #pragma omp master
            {
                iter++;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////
    clock_gettime(CLOCK_MONOTONIC_RAW, &finish);

    std::cout << "\nResult of the program:\n";
    printVect(x, size);


    std::cout << "\nTime: " << (finish.tv_sec - start.tv_sec + 0.000000001 * (finish.tv_nsec - start.tv_nsec)) << '\n';
    std::cout << "Number of iterations: " << iter << '\n';
    #pragma omp parallel
    {
        int n;
        #pragma omp master
        {
            n = omp_get_num_threads();
            std::cout << "Number of threads " << n << '\n';
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////

    delete [] A;
    delete [] b;
    delete [] x;
    delete [] Ax;
    delete [] Ay;

    delete [] tmpVector;
    delete [] y;
    return 0;
}
