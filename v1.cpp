#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <omp.h>

const double epsilon = 0.00001;;

void printVect(double* x, int size) {
    std::cout << '\n';
    for(int i = 0; i < size ; ++ i)
        std::cout << x[i] << '\n';
}

void copyFirstVectorToSecond(const double *vector1, double *vector2, int vectorSize){
#pragma omp parallel for
    for (int i = 0; i < vectorSize; i++) {
        vector2[i] = vector1[i];
    }
}

void mul(const double* partMatrix, const double* vector,  double* res, int size) {
#pragma omp parallel for
    for (int i = 0; i < size; ++i) {
        res[i] = 0.0f;
        for (int j = 0; j < size; ++j) {
            res[i] += partMatrix[i * size + j] * vector[j];
        }
    }
}

double scalarMul(const double* x, const double* y, int size) {
    double mul = 0.0f;
#pragma omp parallel for reduction(+:mul)
    for (int i = 0; i < size; ++i)
        mul += x[i] * y[i];
    return mul;

}
double vectorAbs(const double* x, int size) {
    double len = 0.0f;
#pragma omp parallel for reduction(+:len)
    for (int i = 0; i < size; ++i)
        len += x[i] * x[i];
    return len;
}
void vectorSub(const double* x, const double* y, double* res, double k, int size) {
#pragma omp parallel for
    for(int i = 0; i < size; ++i)
        res[i] = x[i] - k*y[i];
}


double* nextX(double* x, double* partA, const double* b,  double* Ax,  double* Ay, int size) {
    double *y = new double [size];
    vectorSub(Ax, b, y, 1, size);

    mul(partA, y, Ay, size);

    double t;
    t = scalarMul(y, Ay, size) / scalarMul(Ay, Ay, size);


    double* part_x_plus_one = new double[size];
    vectorSub(x, y, part_x_plus_one, t, size);
    copyFirstVectorToSecond(part_x_plus_one, x, size);
    delete [] part_x_plus_one;

    delete [] y;
    return x;
}

bool ending(double* x, double*  A, double* b, double* Ax, int size){
    mul(A, x, Ax, size);

    double* sub = new double [size];
    vectorSub(Ax, b, sub, 1, size);

    double div;
    div = (vectorAbs(sub, size) / vectorAbs(b, size));
    bool res = (div < epsilon * epsilon);

    delete [] sub;

    return res;
}

void init(double* &x, double* &A, double* &b, double* &Ax, double* &Ay, int size) {
    ///////////////////////////////////////////////////////////////////////////////////
    srand(13243);
    double max = 2.35, min = -2.8;

    A = new double[size * size];
    for (int i = 0; i < size; ++i) {
        for (int j = i; j < size; ++j) {
            if (j == i) {
                A[i * size + j] = (double)rand()/(double) RAND_MAX*(max-min)+min;
                A[i * size + j] = A[i * size + j]*A[i * size + j]+100.0;
            } else
                A[i * size + j] = (double)rand()/(double) RAND_MAX*(max-min)+min;
            A[j * size + i] = A[i * size + j];
        }
    }


    double *u = new double[size];
    b = new double[size];
    for (int i = 0; i < size; ++i)
        u[i] = sin((2.0*3.1415926535*i)/(double)size);
    std::cout << "\nVector X should look like this:\n";
    printVect(u, size);
    std::cout << "End of vector.\n";


    mul(A, u, b, size);

    x = new double [size];
    for(int i = 0; i < size; ++i) {
        x[i] = 0.0f;
    }
    ///////////////////////////////////////////////////////////////////////////////////
    Ax = new double [size];
    Ay = new double [size];
    ///////////////////////////////////////////////////////////////////////////////////
    delete[] u;
}



int main(int argc, char *argv[]) {

    int size = 1024; //Размер матрицы и вектора
    double* A; //часть матрицы коэффицентов для каждого процесса она своя
    double* b; //вектор правых частей
    double* x; //вектор значений

    double* Ax; //вспомогательный вектор, хранит в себе результат умножения матрицы A на вектор x
    double* Ay; //вспомогательный вектор, хранит в себе результат умножения матрицы A на вектор y
    ///////////////////////////////////////////////////////////////////////////////////
    init(x, A, b, Ax, Ay, size);
    ///////////////////////////////////////////////////////////////////////////////////

    struct timespec start, finish;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    int iter = 0;
    while(!ending(x, A, b, Ax, size) && iter < 15000) {
        x = nextX(x, A, b,  Ax, Ay, size);
        iter++;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &finish);

    std::cout << "\nResult of the program:\n";
    printVect(x, size);

    std::cout << "\nTime: " << (finish.tv_sec - start.tv_sec + 0.000000001 * (finish.tv_nsec - start.tv_nsec)) << '\n';
    std::cout << "Number of iterations: " << iter << '\n';

    #pragma omp parallel
    {
        int n;
        #pragma omp master
        {
            n = omp_get_num_threads();
            std::cout << "Number of threads " << n << '\n';
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////

    delete [] A;
    delete [] b;
    delete [] x;
    delete [] Ax;
    delete [] Ay;

    return 0;
}
